# XSS Lab 

## Quickstart

* `$ pip install flask`
* `$ git clone https://gitlab.com/thesis2021/xss-lab.git`
* `$ cd xss-lab`
* `$ export FLASK_ENV=development`
* `$ flask run --host=0.0.0.0`
* Open [http://127.0.0.1:5000](http://127.0.0.1:5000).
 
## Videos

The lab also includes tutorial videos for each XSS attack scenario (see [/videos](https://gitlab.com/thesis2021/xss-lab/-/tree/master/videos)). The videos are made available under the Creative Commons CC0 1.0 Universal Public Domain Dedication.

## License

GNU General Public License v3.0
