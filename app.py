from flask import Flask, render_template, request
import db

app = Flask(__name__)

@app.route('/')
def index():
   return app.send_static_file('index.html')

@app.route('/1/search')
def example1():
  term = request.args.get('term')
  comments = db.get_comments(term)
  return render_template('1search.html', comments=comments, term=term)

@app.route('/2/comment', methods=['GET', 'POST'])
def example2():
  if request.method == 'POST':
    db.add_comment(request.form['comment'])

  comments = db.get_comments()
  return render_template('2comment.html', comments=comments)

@app.route('/3/article')
def example3():
   return app.send_static_file('3article.html')

@app.route('/4/search')
def example4():
  term = request.args.get('term')
  comments = db.get_comments(term)
  return render_template('4search.html', comments=comments, term=term)

@app.route('/5/search')
def example5():
  term = request.args.get('term')
  results = db.num_results(term)
  return render_template('5search.html', term=term, results=results)

@app.route('/6/search')
def example6():
  term = request.args.get('term')
  if term is None:
    return app.send_static_file('6search.html')
  comments = db.get_comments(term)
  return render_template('6search.html', term=term, comments=comments)

@app.route('/7/search')
def example7():
  term = request.args.get('term')
  if term is not None:
    term = term.replace("'", "\\'")
  comments = db.get_comments(term)
  return render_template('7search.html', term=term, comments=comments)

@app.route('/8/search')
def example8():
  term = request.args.get('term')
  if term is None:
    return app.send_static_file('8search.html')
  term = term.replace("'", '')
  comments = db.get_comments(term)
  return render_template('8search.html', term=term, comments=comments)

@app.route('/9/search')
def example9():
  term = request.args.get('term')
  comments = db.get_comments(term)
  resp = app.make_response(render_template('9search.html', comments=comments, term=term))
  resp.set_cookie('sessionToken', 'abc123')
  return resp

@app.route('/10/search')
def example10():
  term = request.args.get('term')
  comments = db.get_comments(term)
  resp = app.make_response(render_template('10search.html', comments=comments, term=term))
  resp.headers.set('Content-Security-Policy', 
                   "default-src 'self'; script-src 'self'; object-src 'none'; frame-src 'none'; base-uri 'none'")
  return resp

@app.route('/11/search')
def example11():
  secret_value = "50Y56FEHO3Q4H9I0"
  term = request.args.get('term')
  if term is None:
    resp = app.make_response(render_template('_11search.html', secret_value=secret_value)) 
  else:
    comments = db.get_comments(term)
    resp = app.make_response(render_template('11search.html', term=term, comments=comments, secret_value=secret_value))
  # comment out the following line to disable CSP
  resp.headers.set('Content-Security-Policy', "script-src 'self'")
  return resp

