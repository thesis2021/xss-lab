import SimpleHTTPServer
import SocketServer

PORT = 80

class ServerHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def do_POST(self):
      content_len = int(self.headers.getheader('content-length', 0))
      post_body = self.rfile.read(content_len)
      print self.headers
      print post_body

Handler = ServerHandler

httpd = SocketServer.TCPServer(("", PORT), Handler)

print "Serving HTTP on 0.0.0.0 port 80 ...."
httpd.serve_forever()
